package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import com.example.demo.model.Employees;

public interface EmployeeService {

	public List<Employees> findAll() throws Exception;

	public Optional<Employees> findById(Long id) throws Exception;

	public Employees save(Employees stock) throws Exception;

	public void deleteById(Long id) throws Exception;
	
	public Employees findByName(String name) throws Exception;
}