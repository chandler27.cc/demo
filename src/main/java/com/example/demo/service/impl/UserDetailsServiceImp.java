package com.example.demo.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.controller.LoggingAccessDeniedHandler;
import com.example.demo.model.Employees;
import com.example.demo.service.EmployeeService;


@Service
public class UserDetailsServiceImp implements UserDetailsService {

	private static Logger log = LoggerFactory.getLogger(LoggingAccessDeniedHandler.class);

	@Autowired
	private EmployeeService employeeService;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		Employees emp = new Employees();
		UserDetails userDetails = null;
		log.info("username:" + username);
		try {
			emp = employeeService.findByName(username);
		} catch (Exception e) {
			log.error(e.toString());
		}
		if (username != null) {
			log.info("pdw:" + emp.getPassword());
			Collection<GrantedAuthority> authList = getAuthorities();
			userDetails = new User(username, emp.getPassword().toLowerCase(), true, true, true, true, authList);
		} else {
			throw new UsernameNotFoundException("User not found.");
		}

		return userDetails;
	}

	private Collection<GrantedAuthority> getAuthorities() {
		List<GrantedAuthority> authList = new ArrayList<GrantedAuthority>();
		authList.add(new SimpleGrantedAuthority("ROLE_USER"));
		authList.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
		return authList;
	}

}