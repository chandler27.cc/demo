package com.example.demo.service.impl;


import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.EmployeesRepository;
import com.example.demo.model.Employees;
import com.example.demo.service.EmployeeService;

import lombok.RequiredArgsConstructor;

@Service

@RequiredArgsConstructor
public class EmployeeServiceImpl implements EmployeeService{
	
	@Autowired
    private EmployeesRepository employeesRespository;

	@Override
    public List<Employees> findAll() {
        return employeesRespository.findAll();
    }

	@Override
    public Optional<Employees> findById(Long id) {
        return employeesRespository.findById(id);
    }

	@Override
    public Employees save(Employees stock) {
        return employeesRespository.save(stock);
    }

	@Override
    public void deleteById(Long id) {
        employeesRespository.deleteById(id);
    }

	@Override
	public Employees findByName(String name) throws Exception {
		return employeesRespository.findByName(name);
	}
}