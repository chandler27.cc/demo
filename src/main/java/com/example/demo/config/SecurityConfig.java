package com.example.demo.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.example.demo.controller.LoggingAccessDeniedHandler;
import com.example.demo.service.impl.UserDetailsServiceImp;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	private static Logger log = LoggerFactory.getLogger(SecurityConfig.class);

	@Autowired
	private LoggingAccessDeniedHandler accessDeniedHandler;

	@Bean
	public UserDetailsService userDetailsService() {
		return new UserDetailsServiceImp();
	}

	@Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {

		log.info("inside SecurityConfig");
//		http.authorizeRequests().antMatchers("/", "/js/**", "/css/**", "/img/**", "/webjars/**").permitAll()
//				.antMatchers("/user/**").hasRole("USER").anyRequest().authenticated()
//				.and().formLogin()
//				.loginPage("/login").permitAll()
//				.and().logout().invalidateHttpSession(true).clearAuthentication(true)
//				.logoutRequestMatcher(new AntPathRequestMatcher("/logout")).logoutSuccessUrl("/login?logout")
//				.permitAll()
//				.and().exceptionHandling().accessDeniedHandler(accessDeniedHandler)
//				.and().csrf().disable();
		http.authorizeRequests()
		.antMatchers("/**", "/js/**", "/css/**", "/img/**", "/webjars/**").permitAll()
        .anyRequest().authenticated()
        .and()
        .httpBasic()
        .and().csrf().disable();   
	}

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService()).passwordEncoder(bCryptPasswordEncoder());
	}
	
	 @Override
	    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
	        auth.inMemoryAuthentication()
	                .withUser("user")
	                    .password("password")
	                    .roles("USER")
	            .and()
	                .withUser("manager")
	                    .password("password")
	                    .credentialsExpired(true)
	                    .accountExpired(true)
	                    .accountLocked(true)
	                    .authorities("WRITE_PRIVILEGES", "READ_PRIVILEGES")
	                    .roles("MANAGER");
	    }
	
}