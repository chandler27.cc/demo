package com.example.demo.config;

import com.example.demo.model.Employees;

public interface UserContext {
	Employees getCurrentUser();
	void setCurrentUser(Employees employees);
}