package com.example.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.model.Employees;

public interface EmployeesRepository extends JpaRepository<Employees, Long> {

	@Query("select e from Employees e where e.name = ?1")
	Employees findByName(String name);
}
