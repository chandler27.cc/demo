package com.example.demo.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class MainController {

	@Autowired
	private EmployeeService employeeService;

	@RequestMapping("/hello")
	public ModelAndView hello() {
		return new ModelAndView("hello"); // 根據view resolver mapping至hello.jsp
	}

	@RequestMapping("/")
	public ModelAndView login() {
		return new ModelAndView("login");
	}

	@GetMapping("/save")
	public ResponseEntity<Employees> create(@RequestParam int name, @RequestParam int password) {
		Employees emp = new Employees();
		emp.setName("No." + name);
		emp.setPassword("" + password);
		return ResponseEntity.ok(employeeService.save(emp));
	}

	@GetMapping("/update")
	public ResponseEntity<Employees> update(@RequestParam int id, @RequestParam int name, @RequestParam int password) {

		Optional<Employees> emp = employeeService.findById(Long.valueOf(id));
		if (emp.isPresent()) {
			Employees e = emp.get();
			e.setName(name + "");
			e.setPassword(password + "");
			return ResponseEntity.ok(employeeService.save(emp.get()));
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

	}

	@GetMapping("/delete")
	public ResponseEntity<Employees> delete(@RequestParam int id) {

		Optional<Employees> emp = employeeService.findById(Long.valueOf(id));
		if (emp.isPresent()) {
			employeeService.deleteById(Long.valueOf(id));
			return ResponseEntity.ok().build();
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

	}
}
