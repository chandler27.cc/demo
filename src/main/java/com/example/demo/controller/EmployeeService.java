package com.example.demo.controller;


import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

@Service

@RequiredArgsConstructor
public class EmployeeService {
	
	@Autowired
    private EmployeesRepository employeesRespository;

    public List<Employees> findAll() {
        return employeesRespository.findAll();
    }

    public Optional<Employees> findById(Long id) {
        return employeesRespository.findById(id);
    }

    public Employees save(Employees stock) {
        return employeesRespository.save(stock);
    }

    public void deleteById(Long id) {
        employeesRespository.deleteById(id);
    }
}