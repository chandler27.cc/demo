package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.model.Employees;

@Controller
public class LoginController {

	@RequestMapping("/hello")
	public ModelAndView hello() {
		return new ModelAndView("hello");
	}

	@RequestMapping("/login")
	public ModelAndView login(Model model) {
		model.addAttribute("employees", new Employees()); 
		return new ModelAndView("login");
	}
	
	@RequestMapping("/index")
	public ModelAndView index(Model model) {
		model.addAttribute("employees", new Employees()); 
		return new ModelAndView("index");
	}
	
	@RequestMapping("/access-denied")
	public ModelAndView accessDenied() {
		return new ModelAndView("access-denied");
	}
	
}
