package com.example.demo.controller.rest;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Employees;
import com.example.demo.service.EmployeeService;

@RestController
public class RestEmployeesController {

    private static Logger log = LoggerFactory.getLogger(RestEmployeesController.class);
	
	@Autowired
	private EmployeeService employeeService;

	@GetMapping("/save")
	public ResponseEntity<Employees> create(@RequestParam int name, @RequestParam int password) {
		//int for 測試用，實作後會再調整
		Employees emp = new Employees();
		try {
			emp.setName("No." + name);
			emp.setPassword("" + password);
			employeeService.save(emp);
		} catch (Exception e) {
			log.error(e.toString());
		}
		return ResponseEntity.ok().build();
	}

	@GetMapping("/update")
	public ResponseEntity<Employees> update(@RequestParam int id, @RequestParam int name, @RequestParam int password) {
		//int for 測試用，實作後會再調整
		try {
			Optional<Employees> emp = employeeService.findById(Long.valueOf(id));
			if (emp.isPresent()) {
				Employees e = emp.get();
				e.setName(name + "");
				e.setPassword(password + "");
				employeeService.save(emp.get());
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			log.error(e.toString());
		}
		return ResponseEntity.ok().build();
	}

	@GetMapping("/delete")
	public ResponseEntity<Employees> delete(@RequestParam int id) {
		try {
			Optional<Employees> emp = employeeService.findById(Long.valueOf(id));
			if (emp.isPresent()) {
				employeeService.deleteById(Long.valueOf(id));
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			log.error(e.toString());
		}
		return ResponseEntity.ok().build();
	}
}
