package com.example.demo.controller.rest;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Employees;
import com.example.demo.service.EmployeeService;

@RestController
public class RestLoginController {

	private static Logger log = LoggerFactory.getLogger(RestLoginController.class);

	@Autowired
	private EmployeeService employeeService;

	@PostMapping("/rest/signin")
	public String signin(@Valid @RequestBody Employees employees) {
		try {
			Employees e = employeeService.findByName(employees.getName());
			if (e == null) {
				return "employees not found";
			} else if (!e.getPassword().equals(employees.getPassword())) {
				return "error password";
			}
		} catch (Exception e) {
			log.error(e.toString());
		}
		return "OK";
	}
}
